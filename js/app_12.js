"use strict";

const buttons = document.querySelectorAll(".btn");

window.addEventListener("keydown", (event) => {
  const key = event.key.toUpperCase();

  const button = Array.from(buttons).find((b) => b.textContent === key);

  if (button) {
    if (button.style.backgroundColor === "blue") {
      button.style.backgroundColor = "";
    } else {
      buttons.forEach((b) => (b.style.backgroundColor = ""));

      button.style.backgroundColor = "blue";
    }
  }
});
